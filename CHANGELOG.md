EV3WebController project news

0.3 (2015-10-31)
* detect if we are running on the EV3 as root;
* improved documentation for the installation.

0.2 (2015-10-29)
* Beta release. It is possible to control the robot in four directions;
* minor improvements and bug fixed.

0.1 (2013-04-15)
* First working prototype.
